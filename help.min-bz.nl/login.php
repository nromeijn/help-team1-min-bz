<?php
require("functions.php");

if((isset($_GET["action"]))) {
	if($_GET["action"]=="logout") {
	 	$_SESSION = array();
		setcookie(session_name(), '', time() -42000);
		session_destroy();
		header('Location: login.php');
		die();
		 }
	}


	// Controleer de login als het loginformulier opgestuurd is.
if ((isset($_POST['bsn'])) AND (isset($_POST['password']))) 
	{
	 $vars = array(0,$_POST);
	 if($vars[0]==0) {
		check_login($vars[1]['bsn'],$vars[1]['password']);
	}
}
else {
	session_start();
	if(isset($_COOKIE["ambassade"])) { $CKI_ambassade=$_COOKIE["ambassade"]; }
	
	// Als de sessie gestart is, en er is een bericht gestuurd dan bericht opslaan

	if((isset($CKI_ambassade)) AND (isset($_POST["onderwerp"])) AND (isset($_POST["bericht"])) AND (isset($_SESSION["bsn"]))) {
	 	$vars = array(0,$_POST);
		$bsn=$_SESSION["bsn"];
		$onderwerp=$_POST["onderwerp"];
		$bericht=$_POST["bericht"];	
		$conn=initdb();
		$sql="INSERT INTO Berichten(BSN,AmbassadeID,Onderwerp,Bericht) VALUES ('$bsn','$CKI_ambassade','$onderwerp','$bericht')";
                //echo $sql;
		if($conn[1]->query($sql)=== TRUE) {
			$error=array(0=>0,"error"=>"Uw bericht is succesvol verstuurd");
		}
		else {
			$error=array(0=>1,"error"=>"Error: " . $sql . "<br>" . $conn[1]->error);
		}

	}		
	
	// Is de ambassade gewijzigd? Dan cookie setten.
	if((isset($_GET['ambassade'])) AND ($_GET['ambassade'] != $_COOKIE["ambassade"])) { 
		setcookie("ambassade",$_GET['ambassade'],time()+(86400*30),"/");
		$CKI_ambassade=$_GET['ambassade']; 
	 	}
	} 

require("header.php");
?>

	<div id="threeleft"> &nbsp; 
	<?php if(isset($_SESSION["bsn"])) { include("templates/left_menu.php");}?>
        </div>
	<div id="threecenter">
        <?php if(!isset($_SESSION["bsn"])){ include("templates/login_form.html");}?>
	<?php if(isset($_SESSION["bsn"])) { include("templates/main_menu.php");}?>
	</div>
        <div id="threeright">&nbsp;
	<?php if(isset($_SESSION["bsn"])) { include("templates/right_bar.html");} ?>
	</div>
  </body>
</html>
