<?php
  	$conn=initdb();
        $sql="SELECT ID,ambassade FROM Ambassade";
        $result=$conn[1]->query($sql);
        if($result->num_rows > 0) { $ambassades=$result; }
?>

	<p>Welkom. U bent ingelogd</p>
	<p>Kies hieronder de ambassade waarnaar u een bericht wilt sturen.</p>
	<form>
		<SELECT id="ambassade" onchange="setAmbassade(this)">
		<?php
		 while($row = $ambassades->fetch_assoc()) {
		    $output="<OPTION value=\"" . $row["ID"] . "\"";
		    if ($row["ID"] == $CKI_ambassade) { $output.=" SELECTED ";}
		    $output.=">" . $row["ambassade"] . "</option>";
		    echo $output;
		}
		?>
		</SELECT>
	</form>

