<?php
require("header.php");
?>
  <div id="left">
	<h2>Welkom op de hulp website van ambassade Nederland.</h2><br>
	<p>
	  Nederlanders reizen de hele wereld over. Of dat nu is om de hele wereld te verkennen of zaken te doen, Nederlanders vindt je overal.  Helaas gaan deze reizen niet altijd zoals gehoopt. </p>

	<img id="koenders" src="koenders.jpg" alt="koenders.jpg">
	<p> 
	  Zo kan het zijn dat u onbedoeld in de problemen komt, bijvoorbeeld door onwetendheid over de lokale gebruiken of door toedoen van anderen.  Deze website is speciaal in het leven geroepen om de hulpbehoevende Nederlander in het buitenland bij te staan.
	</p>
	<p>
          In het formulier aan de rechterzijde kunt u een account aanmaken op help.min-bz.nl. Via dit portaal kunt u communiceren met een ambassade bij u in de buurt. Zij kunnen u dan verder helpen. Heeft u al een account? <a href="login.php">Log direct in.</a>
	</p>
	<p>
	<p id="signature"> Bert Koenders, <br><br>Minister van Buitenlandse zaken en koninkrijk relaties
	</p>
  </div>
  <div id="right_center">
    <div id="formulier">
      <h2> Hulpformulier</h2>
	<form action="processed.php" method="POST">
	<table>
	 <tr>
	    <td>Achternaam</td>
	    <td><input type="text" name="voornaam"></input> </td>
	    <td>Voornaam:</td>
	    <td><input type="text" name="achternaam"></input> </td>
	</tr>
	<tr>
	    <td>Straatnaam</td>
	    <td><input type="text" name="straat"></input> </td>
	    <td>Huisnummer:</td>
	    <td><input type="text" name="nummer" size="3"></input> </td>
	</tr>
	<tr>
	    <td>Stad:</td>
	    <td><input type="text" name="Stad" size="20"></input> </td>
	    <td>Postcode:</td>
	    <td><input type="text" name="postcode" size="6"></input> </td>
	</tr>
	<tr>
	    <td>Telefoon</td>
	    <td colspan="2"><input type="text" name="telefoon" size="12"></input> </td>
	</tr>
	<tr>
	    <td colspan="1">BSN nummer</td>
	    <td colspan="2"><input type="text" name="bsn" size="8"></input> </td>
	</tr>
<tr>
	    <td>E-mail adres</td>
	    <td colspan="3"><input type="text" name="email" size="25"></input> </td>
	</tr>
	<tr rowspan="2"><td colspan="3">&nbsp;</td></tr>
	<tr>    
	<td><input type="submit" name="submit" value="Verstuur"></input></td>
	    <td colspan="2"></td>
	</tr>
      </table>
      </form>
   </div>
</div>
  </body>
</html>
